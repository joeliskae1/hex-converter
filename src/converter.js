/**
 * Padding hex component if necessary.
 * Hex component representation requires
 * two hexadesimal characters.
 * @param {string} comp 
 * @returns {string} two hexadecimal characters.
 */
const pad = (comp) => {
    let padded = comp.length == 2 ? comp : "0" + comp;
    return padded;
};

/**
 * RGB-to-HEX conversion
 * @param {number} r RED 0-255
 * @param {number} g GREEN 0-255
 * @param {number} b BLUE 0-255
 * @returns {string} in hex color format, e.g., "#00ff00" (green)
 */
export const rgb_to_hex = (r,g,b) => {
    let hex ="#";
    const HEX_RED = r.toString(16);
    const HEX_GREEN = g.toString(16);
    const HEX_BLUE = b.toString(16);
    return hex + pad(HEX_RED) + pad(HEX_GREEN) + pad(HEX_BLUE);
};

export const hex_to_rgb = (hex) => {

    // Jaotellaan väreille HEX arvot
    const HEX_RED = hex.substring(0, 2);
    const HEX_GREEN = hex.substring(2, 4);
    const HEX_BLUE = hex.substring(4, 6);

    // Muutetaan arvot
    const RED = parseInt(HEX_RED, 16);
    const GREEN = parseInt(HEX_GREEN, 16);
    const BLUE = parseInt(HEX_BLUE, 16);

    return { r: RED, g: GREEN, b: BLUE };
};